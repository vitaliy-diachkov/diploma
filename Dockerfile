FROM python:3.10

ENV PYTHONUNBUFFERED 1

# Update system libraries
RUN apt-get update && apt-get upgrade -y

# Update python package installer
RUN pip install --upgrade pip setuptools wheel

# Installing poetry for dependency management
RUN pip install "poetry==1.1.12"

# Copying poetry's config files
COPY ./pyproject.toml ./poetry.lock /

# Configuring poetry to install dependencies into system python folder
RUN poetry config virtualenvs.create false

# Installing dependencies in non-interactive mode
RUN poetry install --no-interaction

# Install spacy lib dependencies
RUN spacy download uk_core_news_lg

# Copy project files
COPY . /web
WORKDIR /web

ENV LC_ALL "C.UTF-8"
ENV LANG "C.UTF-8"

# Run project entrypoint
ENTRYPOINT ["/web/entrypoint.bash"]
