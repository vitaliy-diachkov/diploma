from django.contrib import admin

from app.coordinating.models import Application

admin.site.register(Application)
