from django.apps import AppConfig


class CoordinatingConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "app.coordinating"
    label = "coordinating"
