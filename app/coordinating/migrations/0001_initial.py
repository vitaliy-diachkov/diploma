# Generated by Django 4.1.4 on 2022-12-10 15:53

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UserRequest',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('title', models.CharField(max_length=128, verbose_name='Title')),
                ('description', models.CharField(max_length=128, verbose_name='Description')),
                ('status', models.CharField(choices=[], max_length=18, verbose_name='Status')),
                ('is_urgent', models.BooleanField(default=False, verbose_name='Is urgent?')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_requests', to=settings.AUTH_USER_MODEL, verbose_name='Author')),
            ],
            options={
                'verbose_name': 'User Request',
                'verbose_name_plural': 'User Requests',
                'db_table': 'user_requests',
            },
        ),
        migrations.CreateModel(
            name='UserProposal',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('title', models.CharField(max_length=128, verbose_name='Title')),
                ('description', models.CharField(max_length=128, verbose_name='Description')),
                ('status', models.CharField(choices=[], max_length=18, verbose_name='Status')),
                ('is_urgent', models.BooleanField(default=False, verbose_name='Is urgent?')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_proposals', to=settings.AUTH_USER_MODEL, verbose_name='Author')),
            ],
            options={
                'verbose_name': 'User Proposal',
                'verbose_name_plural': 'User Proposals',
                'db_table': 'user_proposals',
            },
        ),
    ]
