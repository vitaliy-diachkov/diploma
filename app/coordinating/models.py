from datetime import datetime, timedelta

from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from model_utils.models import TimeStampedModel


def get_application_expire_time() -> datetime:
    """Application shall expire after configured days amount."""
    return timezone.now() + timedelta(
        days=settings.APPLICATION_EXPIRATION_DAYS,
    )


class ApplicationType(models.TextChoices):
    """User help application type."""

    REQUEST = "request", _("Request")
    PROPOSAL = "proposal", _("Proposal")


class ApplicationStatus(models.TextChoices):
    """User help application status."""

    PENDING = "pending", _("Pending")
    APPROVED = "approved", _("Approved")
    REJECTED = "rejected", _("Rejected")
    CLOSED = "closed", _("Closed")
    COMPLETED = "completed", _("Completed")
    IN_PROGRESS = "in_progress", _("In Progress")
    UNPUBLISHED = "unpublished", _("Unpublished")


class ApplicationCategory(models.TextChoices):
    """Application available categories."""

    TRANSPORT = "transport", _("Transport")
    HOUSING = "housing", _("Housing")
    HUMANITARIAN = "humanitarian", _("Humanitarian help")
    GOODS_SERVICES_AND_RESOURCES = "goods", _("Goods, services and resources")
    OTHER = "other", _("Other")


class Application(TimeStampedModel):
    """User help application (request or proposal) model.."""

    author = models.ForeignKey(
        "users.User",
        on_delete=models.CASCADE,
        related_name="applications",
        verbose_name=_("Author"),
    )
    title = models.CharField(
        max_length=128,
        verbose_name=_("Title"),
    )
    description = models.CharField(
        max_length=1024,
        verbose_name=_("Description"),
    )
    status = models.CharField(
        max_length=18,
        choices=ApplicationStatus.choices,
        default=ApplicationStatus.PENDING,
        verbose_name=_("Status")
    )
    is_urgent = models.BooleanField(
        default=False,
        verbose_name=_("Is urgent?"),
    )
    type = models.CharField(
        max_length=18,
        verbose_name=_("Application Type"),
    )
    published = models.DateTimeField(
        null=True,
        default=None,
        verbose_name=_("Date and time published"),
    )
    expires = models.DateTimeField(
        default=get_application_expire_time,
        verbose_name=_("Date and time expires"),
    )
    city = models.ForeignKey(
        "geo.City",
        on_delete=models.CASCADE,
        null=True,
        related_name="applications",
        verbose_name=_("Application target city"),
    )
    category = models.CharField(
        max_length=32,
        choices=ApplicationCategory.choices,
        verbose_name=_("Application category"),
    )

    ACTIVE_STATUSES = [
        ApplicationStatus.APPROVED,
        ApplicationStatus.IN_PROGRESS,
    ]

    class Meta:
        verbose_name = _("Application")
        verbose_name_plural = _("Applications")
        db_table = "applications"

    def __str__(self) -> str:
        return f'<Application "{self.title}" ({self.status})>'


class ApplicationSatisfaction(TimeStampedModel):
    """
    User can select other application as the one satisfying his own.

    That is done to save historical data on user manual choices, so that data
    can be used to train the recommender system model.
    """

    satisfied_application = models.ForeignKey(
        "coordinating.Application",
        on_delete=models.CASCADE,
        related_name="satisfactions",
        verbose_name=_("Satisfied application"),
    )
    satisfying_application = models.ForeignKey(
        "coordinating.Application",
        on_delete=models.CASCADE,
        related_name="satisfying",
        verbose_name=_("Satisfying application"),
    )
