from django.db.models import QuerySet

from app.coordinating.models import Application


def get_active_applications() -> QuerySet[Application]:
    """Re-usable selector for getting list of active user applications."""
    return Application.objects.filter(
        status__in=Application.ACTIVE_STATUSES,
    )
