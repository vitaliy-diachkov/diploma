from django.contrib import admin

from app.geo.models import City

admin.site.register(City)
