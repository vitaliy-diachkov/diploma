import geopy.distance

from django.db import models
from django.utils.translation import gettext_lazy as _


class City(models.Model):
    """Generic model for storing city information."""

    name = models.CharField(max_length=64, verbose_name=_("City name"))
    latitude = models.FloatField()
    longitude = models.FloatField()

    class Meta:
        verbose_name = _("City")
        verbose_name_plural = _("Cities")

    def measure_distance(self, other: "City") -> float:
        """Calculate distance between two cities in km."""
        coordinates = self.latitude, self.longitude
        other_coordinates = other.latitude, other.longitude
        return geopy.distance.geodesic(coordinates, other_coordinates).km

    def __str__(self) -> str:
        return f"<City: {self.name}>"
