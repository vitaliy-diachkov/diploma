from django.contrib import admin

from app.recommender.models import Recommendation

admin.site.register(Recommendation)
