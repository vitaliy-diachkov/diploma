from decimal import Decimal

from django.core import validators
from django.db import models
from django.utils.translation import gettext_lazy as _
from model_utils.models import TimeStampedModel


class Recommendation(TimeStampedModel):
    application = models.ForeignKey(
        "coordinating.Application",
        on_delete=models.CASCADE,
        related_name="recommendations",
        verbose_name=_("Original application")
    )
    recommended_application = models.ForeignKey(
        "coordinating.Application",
        on_delete=models.CASCADE,
        related_name="recommended_for",
        verbose_name=_("Recommended application"),
    )
    scoring = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        validators=[
            validators.MinValueValidator(Decimal(0.0)),
            validators.MaxValueValidator(Decimal(1.0)),
        ],
        verbose_name=_("Scoring"),
    )

    class Meta:
        verbose_name = _("Recommendation")
        verbose_name_plural = _("Recommendations")
        db_table = "recommendations"
        ordering = ["application_id", "-scoring"]

    def __str__(self) -> str:
        return (
            f"<Recommended Application ({self.recommended_application_id}) "
            f"for Application ({self.application_id}): "
            f"{self.scoring:.02f}%>"
        )
