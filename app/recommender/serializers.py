from rest_framework import serializers

from app.coordinating.models import Application
from app.users.serializers import UserContactInfoSerializer


class RecommendedApplicationSerializer(serializers.ModelSerializer):
    scoring = serializers.DecimalField(max_digits=10, decimal_places=2)
    author = UserContactInfoSerializer()
    city = serializers.StringRelatedField(source="city.name")

    class Meta:
        model = Application
        fields = [
            "id",
            "title",
            "description",
            "is_urgent",
            "city",
            "type",
            "scoring",
            "author",
        ]
        read_only_fields = fields
