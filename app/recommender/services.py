from dataclasses import dataclass
from operator import attrgetter

import spacy

from django.db.models import Q, QuerySet

from app.coordinating.models import Application
from app.geo.models import City
from app.recommender.models import Recommendation

nlp = spacy.load("uk_core_news_lg")


@dataclass(frozen=True)
class RelevanceDTO:
    """
    A DTO for saving application and relevance (similarity) pair when
    generating recommendations.
    """

    application: Application
    relevance: float


class RecommenderService:
    STOPWORDS = [",", ".", "!", ";", "а", "i", "й"]
    MAX_DISTANCE = 50.0  # in km

    def generate_recommendations(
        self,
        application: Application,
        active_applications: QuerySet[Application]
    ) -> None:
        relevant_applications = self._get_relevant_applications(
            application=application,
            active_applications=active_applications,
        )

        top_5 = sorted(
            relevant_applications,
            key=attrgetter("relevance"),
            reverse=True
        )[:5]

        self._save_relevant_applications_to_database(
            application,
            relevant_applications=top_5,
        )

    def calculate_relevance(
        self,
        application: Application,
        other_application: Application,
    ) -> float:
        description_text_similarity = self._get_text_similarity(
            application.description,
            other_application.description
        )
        title_text_similarity = self._get_text_similarity(
            application.title,
            other_application.title,
        )
        city_similarity = self._get_city_similarity(
            application.city,
            other_application.city,
        )
        is_urgent_similarity = self._get_is_urgent_similarity(
            application.is_urgent,
            other_application.is_urgent,
        )
        return (
            title_text_similarity * 0.2
            + description_text_similarity * 0.6
            + city_similarity * 0.15
            + is_urgent_similarity * 0.05
        )

    def _get_relevant_applications(
        self,
        application: Application,
        active_applications: QuerySet[Application]
    ) -> list[RelevanceDTO]:
        possible_recommendations = active_applications.exclude(
            Q(type=application.type) | Q(author=application.author),
        )
        return [
            RelevanceDTO(
                application=suggested_application,
                relevance=self.calculate_relevance(
                    application,
                    suggested_application
                ),
            )
            for suggested_application in possible_recommendations
        ]

    def _get_text_similarity(self, text: str, other: str) -> float:
        vector = nlp(self._prepare_text(text))
        other_vector = nlp(self._prepare_text(other))
        return vector.similarity(other_vector)

    def _get_city_similarity(self, city: City, other_city: City) -> float:
        distance = city.measure_distance(other_city)
        if distance == 0.0:
            return 1.0
        elif distance >= self.MAX_DISTANCE:
            return 0.0
        else:
            return distance / self.MAX_DISTANCE

    def _get_is_urgent_similarity(
        self,
        is_urgent: bool,
        other_is_urgent: bool,
    ) -> float:
        return float(is_urgent == other_is_urgent)

    def _prepare_text(self, text: str) -> str:
        return " ".join(filter(
            lambda token: token not in self.STOPWORDS,
            text.split(),
        ))

    def _save_relevant_applications_to_database(
        self,
        application: Application,
        relevant_applications: list[RelevanceDTO]
    ) -> None:
        if not relevant_applications:
            return

        if not application.recommendations.exists():
            Recommendation.objects.bulk_create([
                Recommendation(
                    application=application,
                    recommended_application=relevant_application.application,
                    scoring=relevant_application.relevance,
                )
                for relevant_application in relevant_applications
            ])
        else:
            to_update = []

            for relevant_application, recommendation in zip(
                relevant_applications,
                application.recommendations.order_by("scoring"),
            ):
                recommendation.recommended_application = (
                    relevant_application.application
                )
                recommendation.scoring = relevant_application.relevance
                to_update.append(recommendation)

            Recommendation.objects.bulk_update(
                to_update,
                fields=["recommended_application_id", "scoring"],
            )
