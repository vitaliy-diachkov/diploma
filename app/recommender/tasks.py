from celery import shared_task

from app.coordinating.models import Application
from app.coordinating.selectors import get_active_applications
from app.recommender.models import Recommendation
from app.recommender.services import RecommenderService

__service = RecommenderService()


@shared_task(name="recommender.generate_recommendations")
def generate_recommendations() -> None:
    """
    Generate recommendations for all active applications.
    """
    active_applications = get_active_applications()

    for application in active_applications:
        __service.generate_recommendations(
            application=application,
            active_applications=active_applications,
        )


@shared_task(name="recommender.generate_recommendations_by_application_id")
def generate_recommendations_by_application_id(application_id: int) -> None:
    """
    Generate recommendations for given application ID.
    """
    application = Application.objects.get(id=application_id)
    active_applications = get_active_applications()

    __service.generate_recommendations(
        application=application,
        active_applications=active_applications,
    )


@shared_task(name="recommender.clean_irrelevant_recommendations")
def clean_irrelevant_recommendations() -> None:
    """
    Clean recommendations for applications that are not active anymore.
    """
    Recommendation.objects.exclude(
        application__status__in=Application.ACTIVE_STATUSES,
    ).delete()
