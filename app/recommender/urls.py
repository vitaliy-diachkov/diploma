from django.urls import path

from app.recommender import views

app_name = "recommender"

urlpatterns = [
    path(
        "applications/<int:application_id>/recommendations/",
        views.RecommendationsView.as_view(),
        name="recommendations-list",
    )
]
