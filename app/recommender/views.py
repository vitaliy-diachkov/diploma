from django.db.models import OuterRef, Subquery
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema
from rest_framework import filters
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated

from app.coordinating.models import Application
from app.recommender.models import Recommendation
from app.recommender.serializers import RecommendedApplicationSerializer


@method_decorator(
    name="get",
    decorator=swagger_auto_schema(
        operation_summary="Get list of recommendations for given application",
        operation_description=(
            "This endpoint returns 5 topmost similar applications to the one "
            "was given (proposals for given request and vice versa)"
        ),
        tags=["recommendations"],
    )
)
class RecommendationsView(ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = RecommendedApplicationSerializer

    filter_backends = [filters.OrderingFilter]
    ordering_fields = ["title", "scoring"]
    ordering = ["-scoring"]

    def get_queryset(self):
        author_id = self.request.user.id
        application_id = self.kwargs.get("application_id", 0)

        target_application = get_object_or_404(
            Application,
            id=application_id,
            author_id=author_id,
        )

        return Application.objects.filter(
            id__in=Subquery(
                Recommendation
                .objects
                .filter(application=target_application)
                .values("recommended_application_id")
            )
        ).annotate(
            scoring=Subquery(
                Recommendation
                .objects
                .filter(
                    application_id=target_application.id,
                    recommended_application_id=OuterRef("id"),
                )
                .values("scoring")[:1]
            )
        )
