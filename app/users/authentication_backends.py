from typing import Any, Optional

from django.conf import settings
from django.contrib.auth.backends import ModelBackend
from django.http import HttpRequest
from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import AuthenticationFailed

from app.users.models import User


class CustomAuthenticationHeaderMixin:
    """
    Mixin that implements private _authenticate method that gets the user
    by User ID given in special request header.

    The class is implemented as a mixin so that it could be used with Django
    and Django REST Framework authentication backends.
    """

    def _authenticate(self, request: HttpRequest) -> Optional[User]:
        user_id = request.headers.get(settings.AUTHENTICATION_HEADER_NAME)

        if user_id is not None:
            user = User.objects.filter(id=user_id).first()
            return user if user else None


class HeaderBasedBackend(CustomAuthenticationHeaderMixin, ModelBackend):
    """
    Django's ModelBackend extended to authenticate users via custom
    authentication header that is providing user ID.
    """

    def authenticate(
        self,
        request: HttpRequest,
        **kwargs: Any,
    ) -> Optional[User]:
        return self._authenticate(request)


class HeaderBasedAuthentication(
    CustomAuthenticationHeaderMixin,
    BaseAuthentication
):
    """
    Django REST Framework's BaseAuthentication implementation for
    authenticating users using custom authentication header.
    """

    def authenticate(self, request):
        user = self._authenticate(request)
        if not user:
            raise AuthenticationFailed("No such user")
        return user, None
