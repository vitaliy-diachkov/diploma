from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils.translation import gettext_lazy as _

from app.users.managers import UserManager


class UserType(models.TextChoices):
    """Enumeration for user types."""

    VOLUNTEER = "volunteer", _("Volunteer")
    HELP_RECEIVER = "help_receiver", _("Help receiver")
    ADMIN = "admin", _("Admin")


class User(AbstractBaseUser, PermissionsMixin):
    """Main applications model for storing User profile data."""

    email = models.EmailField(
        unique=True,
        db_index=True,
        verbose_name=_("User contact email"),
    )
    first_name = models.CharField(max_length=32, verbose_name=_("First name"))
    last_name = models.CharField(max_length=32, verbose_name=_("Last name"))
    phone_number = models.CharField(
        max_length=13,
        null=True,
        default=None,
        verbose_name=_("User contact mobile phone number"),
    )
    type = models.CharField(
        max_length=13,
        choices=UserType.choices,
        verbose_name=_("User type"),
    )

    USERNAME_FIELD = "email"

    objects = UserManager()

    class Meta:
        verbose_name = _("User")
        verbose_name_plural = _("Users")
        db_table = "users"

    def __str__(self) -> str:
        return f"<User: {self.email}>"
