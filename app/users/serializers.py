from rest_framework import serializers

from app.users.models import User


class UserContactInfoSerializer(serializers.ModelSerializer):
    """Simple serializer to provide user's contact information."""

    class Meta:
        model = User
        fields = [
            "id",
            "first_name",
            "last_name",
            "email",
            "phone_number",
        ]
