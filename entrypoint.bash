#!/bin/bash -x

StartUp() {
	python /web/manage.py migrate
	python /web/manage.py collectstatic --noinput

	gunicorn app.config.wsgi --bind 0.0.0.0:8000 --env DJANGO_SETTINGS_MODULE=app.config.settings --timeout 120 --chdir=/web
}

StartUpDev() {
	python3 manage.py runserver 0.0.0.0:8000
}

StartUpCeleryWorker() {
	celery -A app.config.celery worker --loglevel=DEBUG
}

StartUpCeleryBeat() {
	celery -A app.config.celery beat --loglevel=DEBUG
}

Shell() {
	python3 manage.py shell -i python
}

Migrations() {
	python3 manage.py migrate
}

Linters() {
	flake8
}

Tests() {
	pytest
}

Bash() {
	bash
}

case $1 in
	start_up) StartUp ;;
	start_up_dev) StartUpDev ;;
	start_up_celery_worker) StartUpCelery ;;
	start_up_celery_beat) StartUpCeleryBeat ;;
	shell) Shell ;;
	migrations) Migrations ;;
	linters) Linters ;;
	tests) Tests ;;
	bash) Bash ;;
	*) exit 1 ;;
esac
